package exo8;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
 


/*
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;*/

public class RegistreMarin {
	private Map<Integer, List<Marin>> th = new HashMap<>();

	public void addMarin(Marin marin) {
		int key = marin.getDecennie(marin);
		if(th.get(key) == null) {
			List<Marin> marins = new ArrayList<Marin>();
			marins.add(marin);
			th.put(key,  marins);
		}
		else
			th.get(key).add(marin);
	}
	
	public List<Marin> get(int decennie){
		return th.get(decennie);
	}
	
	public int count(int decennie) {
		List<Marin> marins = th.get(decennie);
		return marins.size();
	}
	
	public int count() {
		return th.values().size();
	}

	@Override
	public String toString() {
		return "RegistreMarin [th=" + th + "]";
	}
}
