package exo8;

public  class Marin {  
	    String nom, prenom ;
	    int annee_naiss;
		
	    public Marin (String nouveauNom, String nouveauPrenom,  int nouveauAnnee_naiss) {  
	        this.nom = nouveauNom ;   
	        this.prenom = nouveauPrenom ;   
	        this.annee_naiss = nouveauAnnee_naiss ; 
	        	        
	    }
	    
	     public Marin (String nom,  int nouveauAnnee_naiss) {     
	    	this.prenom =  "" ;   
	        annee_naiss = nouveauAnnee_naiss;   
	     }
	     
	    public void setNom(String nom) {
	    	this.nom = nom;
	    	//System.out.println("Nom : " + nom);
	    }
	    
	    public String getNom() {
	    	return this.nom;
	    }
	    
	    public void setPrenom(String prenom) {
	    	this.prenom = prenom;
	    	//System.out.println("Prenom : " + prenom);
	    }
	    
	    public String getPrenom() {
	    	return this.prenom;
	    }
	    
	    @Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((nom == null) ? 0 : nom.hashCode());
			result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
			result = prime * result + annee_naiss;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Marin other = (Marin) obj;
			if (nom == null) {
				if (other.nom != null)
					return false;
			} else if (!nom.equals(other.nom))
				return false;
			if (prenom == null) {
				if (other.prenom != null)
					return false;
			} else if (!prenom.equals(other.prenom))
				return false;
			if (annee_naiss != other.annee_naiss)
				return false;
			return true;
		}

		public void setAnnee_naiss(int annee_naiss) {
	    	this.annee_naiss = annee_naiss;
	    	//System.out.println("annee_naiss : " + annee_naiss);
	    }
	    
	    public int getAnnee_naiss() {
	    	return this.annee_naiss;
	    }
	    
	    public int getDecennie(Marin marin) {
	    	return (marin.getAnnee_naiss() / 10)*10;
	    }
	    
	   

		@Override
		public String toString() {
			return "Marin [nom=" + nom + ", prenom=" + prenom + ", annee_naiss=" + annee_naiss + "]";
		}	 
	    
	}
