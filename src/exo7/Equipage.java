package exo7;
import java.util.HashSet;
import java.util.Iterator;

public class Equipage {
	
	HashSet<Marin> hs = new HashSet<Marin>();
	
	Iterator<Marin> it = hs.iterator();
	
	int nombreMax;
	
	//Constructeur
	public Equipage(int nombreMax) {
		hs = new HashSet<Marin>(nombreMax);
				
	}
	
	public void addMarin(Marin marin) {
		//if(hs.size()<=nombreMax) {
		hs.add(marin);
		//}
		//else
			//System.out.println("Capacit� maximale de l'�quipage atteinte ! Impossible d'ajouter un membre...");
		
	}
	
	
	
	public boolean isMarinPresent(Marin marin) {
		if(hs.contains(marin)==true) {
			return true;
		}
		else 
			return false;
	}
	
	public boolean removeMarin(Marin marin) {
		if(isMarinPresent(marin)) {
			hs.remove(marin);
			return true;
		}
		else 
			return false;
	}
	
	public void clear(Equipage equipage) {
		hs.clear();
	}

	public int getNombreMarins() {
		return hs.size();
	}
	
	public int getMoyenneSalaire() {
		int somme=0;
		for(Iterator<Marin> it = hs.iterator(); it.hasNext();) {
			Marin m = it.next();
			somme=somme+m.getSalaire();
		}
		return somme/hs.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hs == null) ? 0 : hs.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipage other = (Equipage) obj;
		if (hs == null) {
			if (other.hs != null)
				return false;
		} else if (!hs.equals(other.hs))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Equipage [hs=" + hs + "]";
	}
	
}

