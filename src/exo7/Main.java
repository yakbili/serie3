package exo7;

//import org.yakbili.exo5.Marin;

public class Main {

	public static void main(String[] args) {
		Marin m1 = new Marin(null, 0);
		Marin m2 = new Marin(null, 0);
		Marin m3 = new Marin(null, 0);
		Marin m4 = new Marin(null, 0);
		Marin m5 = new Marin(null, 0);
		
		m1.setNom("bar");
		m1.setPrenom("pagre");
		m1.setSalaire(2000);
		
		m2.setNom("bar");
		m2.setPrenom("pagre");
		m2.setSalaire(2000);
		
		m3.setNom("merou");
		m3.setPrenom("sar");
		m3.setSalaire(3000);
		
		m4.setNom("espadon");
		m4.setPrenom("noir");
		m4.setSalaire(4000);
		
		m5.setNom("cabillaud");
		m5.setPrenom("blanc");
		m5.setSalaire(5000);
		
		Equipage e1 = new Equipage(5);
		
		e1.addMarin(m1);
		e1.addMarin(m2);
		e1.addMarin(m3);
		e1.addMarin(m4);
		e1.addMarin(m5);
		
		

		System.out.println("on verifie que addMarin() fonctionne : ");
		System.out.println(e1);
		
		System.out.println("on verifie que removeMarin() fonctionne : ");
        System.out.println(e1.removeMarin(m4));
		
		System.out.println(e1);
		System.out.println("m4 a bien �t� retir�");

		System.out.println("on verifie que m1 est pr�sent :");
		System.out.println(e1.isMarinPresent(m1));
		
		System.out.println("on v�rie que m4 n'est pas pr�sent (retir� precedemment) :");
		System.out.println(e1.isMarinPresent(m4));
		
		System.out.println("on remarque que m2 n'est pas dans equipage car m1=m2 et que hashSet n'accepte pas les doublons");
 
		System.out.println("nombre de marins (restant apr�s test des methodes) : " + e1.getNombreMarins());
		
		System.out.println("salaire moyen des marins : " + e1.getMoyenneSalaire());
		
		
	}

}
